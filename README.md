# What is this?

This is a project that was developed to present a simple linux device driver

# What do you need?

you need to install:
 - git
 - make
 - Eclipse (Optional, the eclipse environment is available, but it is not mandatory).

# How do I build?

The project has two targets (`debug` and `release`) and you can build the project in two different ways:
 - Entering in the folder: `/scripts` and executing `./build.sh <target>`
 - Open the Eclipse program, import the project and run the respective target.

# Where are the results ?

All the output files, including the executable file, are inserted in the respective  folder of the module:

# How to run the programs?

You should use the general commands `insmod <driver.ko>` and `rmmod <driver>`. To see the output you can run `dmesg`.







